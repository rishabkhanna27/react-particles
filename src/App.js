import React from "react";
import MyParticles from "./MyParticles";
import "./styles.css";

export default function App() {
  return (
    <MyParticles/>
  );
}
